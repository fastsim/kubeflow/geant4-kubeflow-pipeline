"""
Kubeflow Component to digest input parameters into the pipeline
"""
from typing import NamedTuple


# noinspection PyTypeHints
def input_parameters(nCells_z: int, nCells_r: int, nCells_phi: int, min_energy: int, max_energy: int, min_angle: int,
                     max_angle: int, init_dir: str, checkpoint_dir: str, valid_dir: str, gen_dir: str, save_dir: str,
                     katib_files: str) \
        -> NamedTuple('Variable_Details',
                      [('nCells_z', int), ('nCells_r', int), ('nCells_phi', int), ('original_dim', int),
                       ('min_energy', int), ('max_energy', int), ('min_angle', int), ('max_angle', int),
                       ('init_dir', str), ('checkpoint_dir', str), ('conv_dir', str), ('valid_dir', str),
                       ('gen_dir', str), ('save_dir', str), ('katib_files', str)]):
    """
    Handling Input Parameters of the project
    :return: The global input variables
    """
    nCells_z, nCells_r = nCells_z, nCells_r
    nCells_phi = nCells_phi
    min_energy = min_energy
    max_energy = max_energy
    min_angle = min_angle
    max_angle = max_angle
    init_dir = init_dir
    checkpoint_dir = checkpoint_dir
    conv_dir = '<USE IF REQUIRED>'
    valid_dir = valid_dir
    gen_dir = gen_dir
    save_dir = save_dir
    katib_files = katib_files

    return (nCells_z, nCells_r, nCells_phi, nCells_z * nCells_r * nCells_phi, min_energy, max_energy,
            min_angle, max_angle, init_dir, checkpoint_dir, conv_dir, valid_dir, gen_dir, save_dir, katib_files)