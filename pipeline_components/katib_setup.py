"""
This component brings in the KATIB into the Kubeflow Pipeline
"""
from typing import NamedTuple


def model_setup(batch_size: int, original_dim: int, intermediate_dim1: int, intermediate_dim2: int,
                intermediate_dim3: int, intermediate_dim4: int, latent_dim: int, epsilon_std: int, mu: int, epochs: int,
                lr: float, outActiv: str, validation_split: float, wReco: int, wkl: float, ki: str, bi: str,
                earlyStop: bool, checkpoint_dir: str, energies_train_location: str,
                condE_train_location: str, condAngle_train_location: str, condGeo_train_location: str,
                katib_files: str, name_experiment: str) -> NamedTuple('Variable_Details', [('best_model', str)]):
    import kfp
    import kubernetes
    import json
    import yaml
    import os

    def create_experiment(client, yaml_filepath, namespace, name_experiment):
        #
        with open(katib_files + 'katib_template.yaml', 'r') as f:
            template = f.read()
        template = template.replace('EXPERIMENT_NAME_IN_METADATA', name_experiment)
        template = template.replace('NAMESPACE', namespace)
        # template = template.replace('INTERMEDIATE_DIM1', intermediate_dim1)
        # template = template.replace('INTERMEDIATE_DIM2', intermediate_dim2)
        # template = template.replace('INTERMEDIATE_DIM3', intermediate_dim3)
        # template = template.replace('INTERMEDIATE_DIM4', intermediate_dim4)
        # template = template.replace('LATENT_DIM', latent_dim)
        # template = template.replace('EPSILON_STD', epsilon_std)
        # template = template.replace('MU', mu)
        # template = template.replace('EPOCHS', epochs)
        # template = template.replace('OUT_ACTIV', outActiv)
        # template = template.replace('VALIDATION_SPLIT', validation_split)
        # template = template.replace('W_RECO', wReco)
        # template = template.replace('WKL', wkl)
        # template = template.replace('KI', ki)
        # template = template.replace('BI', bi)
        # template = template.replace('EARLY_STOP', earlyStop)
        # template = template.replace('CHECKPOINT_DIR', checkpoint_dir)
        # template = template.replace('ENERGIES_TRAIN_LOCATION', energies_train_location)
        # template = template.replace('CONDE_TRAIN_LOCATION', condE_train_location)
        # template = template.replace('COND_ANGLE_TRAIN_LOCATION', condAngle_train_location)
        # template = template.replace('COND_GEO_TRAIN_LOCATION', condGeo_train_location)
        #
        with open(katib_files + 'katib_updated.yaml', 'w') as f:
            f.write(template)

        print('Load template for submission')
        with open(katib_files + 'katib_updated.yaml', 'r') as f:
            experiment_spec = yaml.load(f, Loader=yaml.FullLoader)
            print(json.dumps(experiment_spec, indent=2))

        client.create_namespaced_custom_object(
            group='kubeflow.org',
            version='v1beta1',
            namespace=namespace,
            plural='experiments',
            body=experiment_spec,
        )

    namespace = kfp.Client().get_user_namespace()
    print('Load incluster config')
    kubernetes.config.load_incluster_config()

    print('Obtain CO client')
    k8s_co_client = kubernetes.client.CustomObjectsApi()
    # client read custom object
    print('Create katib experiment')
    yaml_file = os.listdir(katib_files)
    print(yaml_file)
    create_experiment(k8s_co_client, katib_files + yaml_file[0], namespace, name_experiment)

    def to_csv(data, columns):
        csv_table = ''
        for row in data:
            csv_row = []
            for name in columns:
                csv_row.append(row[name])
            csv_table += ','.join(csv_row) + '\n'
        return csv_table

    def write_results_to_ui(results):
        metadata = {
            'outputs': [
                {
                    'type': 'markdown',
                    'storage': 'inline',
                    'source': '# Optimal trial',
                }, {
                    'type': 'table',
                    'storage': 'inline',
                    'format': 'csv',
                    'header': ['name', 'value'],
                    'source': to_csv(results['parameterAssignments'], ['name', 'value']),
                }, {
                    'type': 'table',
                    'storage': 'inline',
                    'format': 'csv',
                    'header': ['name', 'latest', 'max', 'min'],
                    'source': to_csv(results['observation']['metrics'], ['name', 'latest', 'max', 'min']),
                }]
        }

    global optimal_trial
    import time
    import os
    import shutil
    while True:
        time.sleep(10)
        resource = k8s_co_client.get_namespaced_custom_object(
            group='kubeflow.org',
            version='v1beta1',
            namespace=namespace,
            plural='experiments',
            name=name_experiment
        )

        status = resource['status']
        print(status)
        if 'completionTime' in status.keys():
            if status['completionTime']:
                optimal_trial = status['currentOptimalTrial']
                print('Optimal trial')
                print(json.dumps(optimal_trial, indent=2))
                break
    write_results_to_ui(optimal_trial)
    best_model_path = optimal_trial["bestTrialName"]
    path = os.listdir(checkpoint_dir)
    for i in path:
        if best_model_path in i:
            continue
        else:
            shutil.rmtree(checkpoint_dir + i)
    print(os.listdir(checkpoint_dir)[0])
    shutil.move(f"{checkpoint_dir + best_model_path + best_model_path}/VAE-best.tf", f"{checkpoint_dir}/VAE-best.tf")
    return os.listdir(checkpoint_dir)[0]
