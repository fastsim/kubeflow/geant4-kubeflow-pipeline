"""
Preprocess component helps in processing the data in the right format to feed into the training component
"""
from typing import NamedTuple


def preprocess_new(nCells_z: int, nCells_r: int, nCells_phi: int, original_dim: int, min_energy: int, max_energy: int,
                   min_angle: int, max_angle: int, init_dir: str, checkpoint_dir: str, conv_dir: str, valid_dir: str,
                   gen_dir: str) -> NamedTuple('Variable_Details',
                                               [('energies_train_location', str), ('condE_train_location', str),
                                                ('condAngle_train_location', str), ('condGeo_train_location', str)]):
    """
    Helps in preprocessing the input data for the VAE
    :param nCells_z:
    :param nCells_r:
    :param nCells_phi:
    :param original_dim:
    :param min_energy: Min energy in the training data
    :param max_energy: Max energy in the training data
    :param min_angle: Min angle in the training data
    :param max_angle: Max angle in the training data
    :param init_dir: Directory where all your corresponding results will be saved
    :param checkpoint_dir: Saving the training checkpoints
    :param conv_dir: Directory to save the converted model
    :param valid_dir: Saving the validation results
    :param gen_dir: Saving to generate model
    :return: Returns the path of the training files locations
    """
    import h5py
    import numpy as np
    import os
    import shutil
    energies_Train = []
    condE_Train = []
    condAngle_Train = []
    condGeo_Train = []
    training_data_inputs = init_dir + 'Intermediate_Train'
    shutil.rmtree(training_data_inputs)
    os.mkdir(training_data_inputs)
    # This example is trained using 2 detector geometries
    for geo in ['SciPb']:
        dirGeo = init_dir + geo + '/'
        # loop over the angles in a step of 10
        for angleParticle in range(min_angle, max_angle + 10, 10):
            fName = '%s_angle_%s.h5' % (geo, angleParticle)
            fName = dirGeo + fName
            h5 = h5py.File(fName)
            energyParticle = min_energy
            while energyParticle <= max_energy:
                # scale the energy of each cell to the energy of the primary particle (in MeV units) 
                events = np.array(h5['%s' % energyParticle]) / (energyParticle * 1000)
                energies_Train.append(events.reshape(len(events), original_dim))
                # build the energy and angle condition vectors
                condE_Train.append([energyParticle / max_energy] * len(events))
                condAngle_Train.append([angleParticle / max_angle] * len(events))
                # build the geometry condition vector (1 hot encoding vector)
                if geo == 'SiW':
                    condGeo_Train.append([[0, 1]] * len(events))
                if geo == 'SciPb':
                    condGeo_Train.append([[1, 0]] * len(events))
                energyParticle *= 2
    # return numpy arrays 
    energies_Train = np.concatenate(energies_Train)
    condE_Train = np.concatenate(condE_Train)
    condAngle_Train = np.concatenate(condAngle_Train)
    condGeo_Train = np.concatenate(condGeo_Train)
    energies_train_location = training_data_inputs + '/energies_train.npy'
    np.save(energies_train_location, energies_Train)
    condE_train_location = training_data_inputs + '/condE_train.npy'
    np.save(condE_train_location, condE_Train)
    condAngle_train_location = training_data_inputs + '/condAngle_train.npy'
    np.save(condAngle_train_location, condAngle_Train)
    condGeo_train_location = training_data_inputs + '/condGeo_train.npy'
    np.save(condGeo_train_location, condGeo_Train)
    return energies_train_location, condE_train_location, condAngle_train_location, condGeo_train_location
